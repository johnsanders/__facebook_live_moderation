import {combineReducers} from "redux";
import {
	UPDATE_COMMENT,
	UPDATE_COMMENTS,
	ADD_COMMENTS,
	SET_COMMENTS,
	UNAPPROVE_ALL_COMMENTS,
	DELETE_UNAPPROVED_COMMENTS,
	SET_UPDATE_TO_SERVER_NEEDED
} from "./actions";
import {mergeCommentsKeepOld, mergeCommentsKeepNew, sortCommentsByDate} from "./helpers/utilities";

const comments = (state = [], action) => {
	if (action.type === ADD_COMMENTS) {
		const merged = mergeCommentsKeepOld(state, action.payload);
		const sorted = sortCommentsByDate(merged);
		return sorted;
	} else if (action.type === UPDATE_COMMENT) {
		return state.map( comment => (
			comment.facebookId === action.payload.facebookId 
				? Object.assign(comment, {[action.payload.fieldName]:action.payload.newValue})
				: comment
		));
	} else if (action.type === UPDATE_COMMENTS) {
		const merged = mergeCommentsKeepNew(state, action.payload);
		const sorted = sortCommentsByDate(merged);
		return sorted;
	}	else if (action.type === SET_COMMENTS) {
		return action.payload;
	} else if (action.type === UNAPPROVE_ALL_COMMENTS) {
		return state.map(comment => (
			Object.assign(comment, {moderationStatus:action.payload})
		));
	} else if (action.type === DELETE_UNAPPROVED_COMMENTS) {
		return state.filter( comment => (
			comment.moderationStatus !== action.payload
		));
	} else {
		return state;
	}
};
const updateToServerNeeded = (state = false, action) => (
	action.type === SET_UPDATE_TO_SERVER_NEEDED ? action.payload : state
);

const rootReducer = combineReducers({comments, updateToServerNeeded});

export default rootReducer;
