import {
	UPDATE_COMMENT,
	UPDATE_COMMENTS,
	ADD_COMMENTS,
	SET_COMMENTS,
	SET_UPDATE_TO_SERVER_NEEDED,
	UNAPPROVE_ALL_COMMENTS,
	DELETE_UNAPPROVED_COMMENTS
} from "./actions";
import fbHelpers from "./helpers/FacebookApiHelpers";

export const updateComments = comments => (
	{type:UPDATE_COMMENTS, payload:comments}
);
export const updateComment = (facebookId, fieldName, newValue) => (
	{type:UPDATE_COMMENT, payload:{facebookId, fieldName, newValue}}
);
export const addComments = newComments => (
	{type:ADD_COMMENTS, payload:newComments}
);
export const setComments = comments => (
	{type:SET_COMMENTS, payload:comments}
);
export const setUpdateToServerNeeded = isNeeded => (
	{type:SET_UPDATE_TO_SERVER_NEEDED, payload:isNeeded}
);
export const unapproveAllComments = unapprovedString => (
	{type:UNAPPROVE_ALL_COMMENTS, payload:unapprovedString}
);
export const deleteUnapprovedComments = unapprovedString => (
	{type:DELETE_UNAPPROVED_COMMENTS, payload:unapprovedString}
);
export const updateFromFacebook = (videoId, since) => {
	return dispatch => {
		fbHelpers.getComments(videoId, since)
			.then(response => {
				const newComments = response.data.map(comment => (
					{
						facebookId:comment.id,
						created:new Date(comment.created_time).getTime() + Math.floor(Math.random() * 999),
						numDisplays:0,
						lastOnAir:0,
						onAirNow:false,
						user:comment.from.name,
						userId:comment.from.id,
						comment:comment.message,
						moderationStatus:"unmoderated",
						editActive:false
					}
				));
				dispatch(addComments(newComments));
			})
			.catch(error => console.error(error));
	};
};
