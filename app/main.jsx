import React from "react";
import {render} from "react-dom";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./store";
import AppModerateContainer from "./containers/AppModerateContainer";
import AppCopyeditContainer from "./containers/AppCopyeditContainer";
import "./styles/bootstrap/css/bootstrap.css";
import "./styles/style.css";

render((
	<BrowserRouter basename="/apps/misc/cnn_talk/fb">
		<Provider store={store}>
			<Switch>
				<Route exact path="*/moderate" component={AppModerateContainer} />
				<Route exact path="*/copyedit" component={AppCopyeditContainer} />
			</Switch>
		</Provider>
	</BrowserRouter>
), document.getElementById("app"));

