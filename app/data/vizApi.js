export const apiPath = "http://int-soc-tk-mse:8580/actions/";
export const apiEndpoints = {
	tickerOn: [
		"Main%20ticker%20system%20on",
		"Main%20program%20on",
		"Main%20XMESSAGE%20in"
	],
	tickerOff: [
		"Main%20XMESSAGE%20out",
		"Main%20program%20off",
		"Main%20ticker%20system%20off"
	]
};
