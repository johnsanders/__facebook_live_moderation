import React from "react";
import autobind from "react-autobind";
import isEqual from "lodash-es/isEqual";
import CommentEditable from "../components/CommentEditable";
import {updateComment, setUpdateToServerNeeded} from "../actionCreators";
import {connect} from "react-redux";
import PropTypes from "prop-types";

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
	handleUpdateComment(facebookId, fieldName, newValue) {
		dispatch(updateComment(facebookId, fieldName, newValue));
	},
	handleUpdateToServerNeededChange(isNeeded) {
		dispatch(setUpdateToServerNeeded(isNeeded));
	}
});

class CommentEditableContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {editActive:false, editValue:this.props.value};
		autobind(this);
	}
	shouldComponentUpdate(nextProps, nextState) {
		return this.state.editActive
			? !isEqual(this.state, nextState)
			: !isEqual(this.state, nextState) || !isEqual(this.props, nextProps);
	}
	componentDidUpdate(prevProps, prevState) {
		if (this.state.editActive && !prevState.editActive) {
			document.getElementsByClassName("commentEditable")[0].focus();
		}
	}
	handleEditActivate(e) {
		if (e.type === "click") {
			this.setState({editActive:true, editValue:this.props.value});
		} else if (e.type === "blur") {
			this.props.handleUpdateComment(this.props.facebookId, this.props.fieldName, e.target.value);
			this.props.handleUpdateToServerNeededChange(true);
			this.setState({editActive:false});
		}
	}
	handleEditInteraction(e) {
		if (e.type === "keydown" && e.keyCode === 13) {
			e.target.blur();
		} else if (e.type === "change") {
			this.setState({editValue:e.target.value});
		}
	}
	render() {
		return (
			<CommentEditable
				editActive={this.state.editActive}
				savedValue={this.props.value}
				editValue={this.state.editValue}
				handleEditActivate={this.handleEditActivate}
				handleEditInteraction={this.handleEditInteraction}
			/>
		);
	}
}
CommentEditableContainer.propTypes = {
	facebookId:PropTypes.string.isRequired,
	fieldName:PropTypes.string.isRequired,
	value:PropTypes.string,
	handleUpdateComment:PropTypes.func.isRequired,
	handleUpdateToServerNeededChange:PropTypes.func.isRequired
};
export default connect(mapStateToProps, mapDispatchToProps)(CommentEditableContainer);
