import React from "react";
import autobind from "react-autobind";
import axios from "axios";
import ModerateControls from "../components/ModerateControls";
import VizControls from "../components/VizControls";
import QuestionControlsContainer from "./QuestionControlsContainer";
import {apiPath, apiEndpoints} from "../data/vizApi";
import PropTypes from "prop-types";

class ModerateControlsContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {tickerControlVisible:false, questionControlVisible:false};
		autobind(this);
	}
	toggleControlVisible(e) {
		const name = e.target.getAttribute("name");
		this.setState({[name + "ControlVisible"]:!this.state[name + "ControlVisible"]});
	}
	handleTickerControlClick(e) {
		const btnName = e.target.getAttribute("name");
		const urls = apiEndpoints[btnName].map(endpoint => apiPath + endpoint);
		this.sendVizCommands(urls);
	}
	sendVizCommands(urls) {
		const requests = urls.map((url, index) => (
			new Promise((resolve) => {
				setTimeout(() => {
					resolve();
				}, index * 1000);
			})
			.then(() => axios.post(url))
		));
		axios.all(requests).then(axios.spread((one, two, three) => {
			if (one.statusText==="No content" && two.statusText==="No Content" && three.statusText ==="No Content") {
				this.setState({vizControllerConfirmVisible: true});
				setTimeout(() => this.setState({vizControllerConfirmVisible: false}), 2000);
			}
		}));
	}
	render() {
		return (
			<div style={{marginBottom:"10px"}}>
				<ModerateControls
					toggleControlVisible={this.toggleControlVisible}
					handleTickerControlClick={this.handleTickerControlClick}
					cleanUp={this.props.handleCleanUp}
					clearApprovals={this.props.handleClearApprovals}
					tickerControlVisible={this.state.tickerControlVisible}
					toggleCc={this.props.toggleCc}
					ccConnected={this.props.ccConnected}
				/>
				{
					this.state.tickerControlVisible ?
						<VizControls
							handleTickerControlClick={this.handleTickerControlClick}
						/>
					: null
				}
				<div style={{clear:"both"}} />
				{
					this.state.questionControlVisible ?
						<QuestionControlsContainer />
					: null
				}
			</div>
		);
	}
}
ModerateControlsContainer.propTypes = {
	handleCleanUp:PropTypes.func.isRequired,
	handleClearApprovals:PropTypes.func.isRequired,
	toggleCc:PropTypes.func.isRequired,
	ccConnected:PropTypes.bool.isRequired
};
export default ModerateControlsContainer;
