import React from "react";
import autobind from "react-autobind";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import CommentTable from "../components/CommentTable";
import ModerateControlsContainer from "./ModerateControlsContainer";
import {approvalStrings} from "../helpers/utilities";
import {setUpdateToServerNeeded, unapproveAllComments, deleteUnapprovedComments} from "../actionCreators";

const mapStateToProps = state => ({comments:state.comments});
const mapDispatchToProps = dispatch => ({
	handleUpdateToServerNeededChange(isNeeded) {
		dispatch(setUpdateToServerNeeded(isNeeded));
	},
	handleUnapproveAllComments(unapprovedString) {
		dispatch(unapproveAllComments(unapprovedString));
	},
	handleDeleteUnapprovedComments(unapprovedString) {
		dispatch(deleteUnapprovedComments(unapprovedString));
	}
});

class CommentPanelContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {comments:[]};
		this.unapprovedString = approvalStrings[props.role][0];
		this.approvedString = approvalStrings[props.role][1];
		autobind(this);
	}
	componentWillReceiveProps(newProps) {
		if (newProps.commentIdBeingEdited === null && typeof this.props.commentIdBeingEdited === "string") {
			this.props.handleUpdateToServerNeededChange(true);
		}
	}
	handleClearApprovals() {
		this.props.handleUnapproveAllComments(this.unapprovedString);
		this.props.handleUpdateToServerNeededChange(true);
	}
	handleCleanUp() {
		this.props.handleDeleteUnapprovedComments(this.unapprovedString);
	}
	render() {
		return (
			<div style={{display:"flex", flexDirection:"column"}}>
				{
					this.props.role === "moderate" ?
						<ModerateControlsContainer
							handleClearApprovals={this.handleClearApprovals}
							handleCleanUp={this.handleCleanUp}
							toggleCc={this.props.toggleCc}
							ccConnected={this.props.ccConnected}
						/>
					: null
				}
				<CommentTable
					comments={this.props.comments}
					approve={this.approve}
					role={this.props.role}
				/>
			</div>
		);
	}
}
CommentPanelContainer.propTypes = {
	role:PropTypes.string.isRequired,
	comments:PropTypes.array.isRequired,
	handleUpdateToServerNeededChange:PropTypes.func.isRequired,
	videoId:PropTypes.string,
	toggleCc:PropTypes.func,
	ccConnected:PropTypes.bool,
	commentIdBeingEdited:PropTypes.string,
	handleUnapproveAllComments:PropTypes.func.isRequired,
	handleDeleteUnapprovedComments:PropTypes.func.isRequired
};
export default connect(mapStateToProps, mapDispatchToProps)(CommentPanelContainer);
