import React from "react";
import autobind from "react-autobind"
import QuestionControls from "../components/QuestionControls";
import {sendQuestionToServer, getQuestionFromServer} from "../helpers/serverInterface";

class QuestionControlsContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {title:"", author:"", tab:""};
		autobind(this);
	}
	componentDidMount(){
		getQuestionFromServer().then( res =>
			this.setState(res)
		);
	}
	handleChange(e){
		const name = e.target.getAttribute("name");
		this.setState({[name]:e.target.value});
	}
	handleSave(e){
		e.preventDefault();
		sendQuestionToServer(this.state.title, this.state.author, this.state.tab);
		console.log("thing")
	}
	render(){
		return (
			<QuestionControls
				title={this.state.title}	
				author={this.state.author}
				tab={this.state.tab}
				handleChange={this.handleChange}
				handleSave={this.handleSave}
			/>
		);
	}
};
export default QuestionControlsContainer;
