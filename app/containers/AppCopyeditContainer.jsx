import React from "react";
import CommentApprovalUpdater from "../utils/CommentApprovalUpdater";
import AppCopyedit from "../components/AppCopyedit";

class AppCopyeditContainer extends React.Component {
	constructor(props) {
		super(props);
		this.commentApprovalUpdater = new CommentApprovalUpdater("copyedit");
	}
	componentDidMount() {
		this.commentApprovalUpdater.update();
	}
	render() {
		return <AppCopyedit />;
	}
}

export default AppCopyeditContainer;
