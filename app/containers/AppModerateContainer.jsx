/* global FB */
import React from "react";
import autobind from "react-autobind";
import fbHelpers from "../helpers/FacebookApiHelpers";
import AppModerate from "../components/AppModerate";
import {getModeratedCommentsFromServer, sendLiveUserToServer} from "../helpers/serverInterface";
import SocketIo from "socket.io-client";
import FbCommentUpdater from "../utils/FbCommentUpdater";
import CommentApprovalUpdater from "../utils/CommentApprovalUpdater";
import CommentSequencer from "../utils/CommentSequencer";

class AppModerateContainer extends React.Component {
	constructor(props) {
		super(props);
		this.myLoc = "cnnTalkModeration";
		const fbAppId = "1194656053976330";
		this.state = {liveUser:"cnninternational", videoId:"", ccConnected:false,
			fbLoginStatus:false, updating:false, logging:true, timeToUpdate:0};
		FB.init({appId:fbAppId, version:"v2.7", status:true});
		autobind(this);
		this.fbUpdater = new FbCommentUpdater(this.getState);
		this.commentApprovalUpdater = new CommentApprovalUpdater("moderate");
		this.commentSequencer = new CommentSequencer();
	}
	componentDidMount() {
		getModeratedCommentsFromServer()
			.then(response => {
				this.setState({liveUser:response.liveUser, logging:this.shouldLog(response.liveUser)});
			})
			.then(() => {
				FB.getLoginStatus((res) => {
					if (res.status === "connected") {
						this.setState({fbLoginStatus: true}, () => {
							this.getVideoId();
						});
					}
				});
			});
		this.socketIo = new SocketIo("http://cnnitouch:3000");
		this.socketIo.on("connect", () => {
			this.socketIo.emit("join", "admin");
		});
		this.socketIo.on("ccConnected", isConnected => {
			this.setState({ccConnected:isConnected});
		});
		this.commentApprovalUpdater.update();
	}
	componentWillUnmount() {
		if (this.ws) { this.ws.close(); }
		this.socketIo.disconnect();
	}
	getVideoId() {
		if (this.state.fbLoginStatus) {
			fbHelpers.getLastLiveVideo(this.state.liveUser)
				.then(video => {
					if (video) {
						this.setState({videoId:video.id});
					}
				})
				.catch(err => console.error(err));
		}
	}
	handleInputUpdate(e) {
		let newState = {};
		const propertyToUpdate = e.target.id;
		newState[propertyToUpdate] = e.target.value;
		this.setState(newState, () => {
			if (propertyToUpdate === "updating" && this.state.updating) {
				this.fbUpdater.doUpdate();
			}
		});
	}
	toggleCc() {
		if (this.state.ccConnected) {
			this.socketIo.emit("ccAction", "disconnect");
		} else {
			this.socketIo.emit("ccAction", "connect");
		}
	}
	setLiveUser(e) {
		e.preventDefault();
		this.setState({videoId:null}, () => {
			this.getVideoId();
		});
		this.setState({logging: this.shouldLog(this.state.liveUser)});
		sendLiveUserToServer(this.state.liveUser)
			.then(() => {
			}).catch((err) => console.error(err));
	}
	shouldLog(liveUser) {
		return liveUser === "cnninternational" || liveUser === "cnni";
	}
	getState(key) {
		return this.state[key];
	}
	render() {
		return (
			<AppModerate
				handleInputUpdate={this.handleInputUpdate}
				liveUser={this.state.liveUser}
				setLiveUser={this.setLiveUser}
				videoId={this.state.videoId}
				getVideoId={this.getVideoId}
				updating={this.state.updating}
				logging={this.state.logging}
				ccConnected={this.state.ccConnected}
				toggleCc={this.toggleCc}
				login={fbHelpers.login}
				fbLoginStatus={this.state.fbLoginStatus}
			/>
		);
	}
}
export default AppModerateContainer;
