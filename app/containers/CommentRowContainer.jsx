import React from "react";
import autobind from "react-autobind";
import isEqual from "lodash-es/isEqual";
import CommentRow from "../components/CommentRow";
import {connect} from "react-redux";
import {updateComment, setUpdateToServerNeeded} from "../actionCreators.js";
import PropTypes from "prop-types";

const mapStateToProps = state => ({comments:state.comments});
const mapDispatchToProps = dispatch => ({
	handleUpdateComment(facebookId, fieldName, newValue) {
		dispatch(updateComment(facebookId, fieldName, newValue));
	},
	handleUpdateToServerNeededChange(isNeeded) {
		dispatch(setUpdateToServerNeeded(isNeeded));
	}
});
class CommentRowContainer extends React.Component {
	constructor(props) {
		super(props);
		autobind(this);
		this.maxChars = 135;
	}
	shouldComponentUpdate(nextProps) {
		return !isEqual(nextProps, this.props);
	}
	handleApprove() {
		let newStatus;
		if (this.props.moderationStatus === this.props.unapprovedString) {
			newStatus = this.props.approvedString;
		} else {
			newStatus = this.props.unapprovedString;
		}
		this.props.handleUpdateComment(this.props.facebookId, "moderationStatus", newStatus);
		this.props.handleUpdateToServerNeededChange(true);
	}
	getCountClass(count) {
		if (count > this.maxChars) return "error";
		if (count > this.maxChars - 5) return "warn";
		return "";
	}
	getBkgColor(moderationStatus) {
		switch (moderationStatus) {
		case "unmoderated": return "";
		case "moderated" : return "#fff8ea";
		case "readyForAir" : return "#e2ffdf";
		}
	}
	render() {
		return (
			<CommentRow
				moderationStatus={this.props.moderationStatus}
				user={this.props.user}
				facebookId={this.props.facebookId}
				created={this.props.created}
				comment={this.props.comment}
				numDisplays={this.props.numDisplays}
				onAirNow={this.props.onAirNow}
				handleApprove={this.handleApprove}
				getBkgColor={this.getBkgColor}
				getCountClass={this.getCountClass}
			/>
		);
	}
}

CommentRowContainer.propTypes = {
	moderationStatus:PropTypes.string,
	unapprovedString:PropTypes.string,
	handleUpdateComment:PropTypes.func.isRequired,
	handleUpdateToServerNeededChange:PropTypes.func.isRequired,
	approvedString:PropTypes.string,
	user:PropTypes.string,
	facebookId:PropTypes.string,
	created:PropTypes.number,
	onAirNow:PropTypes.bool,
	numDisplays:PropTypes.number,
	comment:PropTypes.string
};
export default connect(mapStateToProps, mapDispatchToProps)(CommentRowContainer);
