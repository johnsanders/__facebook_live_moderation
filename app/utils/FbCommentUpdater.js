import store from "../store";
import {logEngagementsToServer} from "../helpers/serverInterface";
import {updateFromFacebook} from "../actionCreators";

class FbCommentUpdater {
	constructor(getState) {
		this.interval = 6000;
		this.getState = getState;
		this.lastFbUpdate = Date.now() - 60 * 60 * 1000;
		this.doUpdate = this.doUpdate.bind(this);
	}
	doUpdate() {
		window.clearTimeout(this.timeout);
		if (this.getState("updating") && this.getState("videoId")) {
			const videoId = this.getState("videoId");
			store.dispatch(updateFromFacebook(videoId, this.lastFbUpdate));
			this.lastFbUpdate = Date.now() - 10 * 1000;
			if (this.getState("logging")) {
				logEngagementsToServer(store.getState().comments);
			}
			this.timeout = setTimeout(this.doUpdate, this.interval);
		}
	}
}

export default FbCommentUpdater;
