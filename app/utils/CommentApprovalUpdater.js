import store from "../store";
import {setComments, setUpdateToServerNeeded} from "../actionCreators";
import {sendModeratedCommentsToServer, getModeratedCommentsFromServer} from "../helpers/serverInterface";
import WebSocketConnection from "../helpers/WebSocketConnection";
import {updateComments} from "../actionCreators";

class CommentApprovalUpdater {
	constructor(role) {
		this.listener = this.listener.bind(this);
		this.update = this.update.bind(this);
		this.onWsMessage = this.onWsMessage.bind(this);
		this.role = role;
		store.subscribe(this.listener);
		this.ws = new WebSocketConnection("ws://cnnitouch.turner.com:8082", "cnnTalkModeration", this.onWsMessage);
	}
	listener() {
		if (store.getState().updateToServerNeeded) {
			const comments = store.getState().comments;
			sendModeratedCommentsToServer(comments)
				.then(() => {
					this.ws.send(JSON.stringify({command:"sendSyncToServer", location:"cnnTalkModeration", msg:"reload"}));
					store.dispatch(setUpdateToServerNeeded(false));
				});
		}
	}
	onWsMessage(msg) {
		if (msg === "reload") {
			this.update();
		}
	}
	update() {
		getModeratedCommentsFromServer().then(response => {
			const newComments = response.comments;
			if (this.role === "copyedit") {
				store.dispatch(setComments(newComments));
			} else if (this.role === "moderate") {
				store.dispatch(updateComments(newComments));
			}
		});
	}
}

export default CommentApprovalUpdater;
