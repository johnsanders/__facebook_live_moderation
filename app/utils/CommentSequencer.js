import store from "../store";
import {setComments, setUpdateToServerNeeded} from "../actionCreators";
import {sendModeratedCommentsToServer} from "../helpers/serverInterface";

class CommentSequencer {
	constructor() {
		this.interval = 10000;
		this.maxLiveComments = 13;
		this.run = this.run.bind(this);
		setTimeout(this.run, this.interval);
	}
	run() {
		const comments = store.getState().comments;
		const commentsTrimmed = this.unapproveOldComments(comments, this.maxLiveComments);
		const newComments = this.setNextOnAirComment(commentsTrimmed);
		store.dispatch(setComments(newComments));
		sendModeratedCommentsToServer(newComments)
			.then(() => store.dispatch(setUpdateToServerNeeded(true)));
		setTimeout(this.run, this.interval);
	}
	unapproveOldComments(allComments, maxLiveComments) {
		const onAirComments = allComments.filter(comment => comment.moderationStatus === "readyForAir");
		const otherComments = allComments.filter(comment => comment.moderationStatus !== "readyForAir");
		const onAirSorted = onAirComments.sort((a,b) => b.created - a.created);
		const newOnAirComments = onAirSorted.map((comment, i) => {
			if (i >= maxLiveComments) {
				comment.moderationStatus = "moderated";
			}
			return comment;
		});
		const newComments = newOnAirComments.concat(otherComments);
		const sorted = newComments.sort((a,b) => a.created - b.created);
		return sorted;
	}
	setNextOnAirComment(comments) {
		const currentOnAirComment = comments.find(comment => comment.onAirNow);
		const newComments = comments.slice();
		if (currentOnAirComment) {
			const currentOnAirCommentIndex = comments.indexOf(currentOnAirComment);
			currentOnAirComment.onAirNow = false;
			newComments[currentOnAirCommentIndex] = currentOnAirComment;
		}
		const approvedComments = comments.filter(comment => comment.moderationStatus === "readyForAir");
		if (approvedComments.length > 0) {
			const selectedNextComment = approvedComments.slice().sort((a,b)=>a.lastOnAir - b.lastOnAir)[0];
			const nextOnAirComment = Object.assign({}, selectedNextComment, {
				onAirNow:true,
				numDisplays:selectedNextComment.numDisplays+1,
				lastOnAir:Date.now()
			});
			const nextOnAirCommentIndex = comments.indexOf(selectedNextComment);
			newComments[nextOnAirCommentIndex] = nextOnAirComment;
		}
		return newComments;
	}
}
export default CommentSequencer;
