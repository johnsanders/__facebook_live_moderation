export const encode = (moderatedComments) => {
	const encodedComments = moderatedComments.map((moderatedComment) => {
		let encodedComment = JSON.parse(JSON.stringify(moderatedComment));
		encodedComment.comment = encodeURIComponent(encodedComment.comment);
		delete encodedComment.editActive;
		return encodedComment;
	});
	return JSON.stringify(encodedComments);
};
export const mergeCommentsKeepNew = (oldComments, newComments) => {
	let mergedComments = newComments.slice(0);
	oldComments.forEach((oldComment) => {
		if (!newComments.find((newComment) => newComment.facebookId === oldComment.facebookId)) {
			mergedComments.push(oldComment);
		}
	});
	return mergedComments;
};
const sortAsc = (a,b) => {
	if (a.created === b.created) return 0;
	return a.created > b.created ? 1 : -1;
};
export const sortCommentsByDate = comments => comments.sort(sortAsc);

export const mergeCommentsKeepOld = (oldComments, newComments) => {
	let mergedComments = oldComments.slice(0);
	newComments.forEach((newComment) => {
		if (!oldComments.find((oldComment) => oldComment.facebookId === newComment.facebookId)) {
			mergedComments.push(newComment);
		}
	});
	return mergedComments;
};
export const timestampToHourMinSec = (unix) => {
	const date = new Date(unix);
	const hour = date.getHours().toString();
	let minute = date.getMinutes().toString();
	if (minute.length < 2) minute = "0" + minute;
	let second = date.getSeconds().toString();
	if (second.length < 2) second = "0" + second;
	return `${hour}:${minute}:${second}`;
};
export const approvalStrings = {
	moderate:["unmoderated", "moderated"],
	copyedit:["moderated", "readyForAir"]
};
