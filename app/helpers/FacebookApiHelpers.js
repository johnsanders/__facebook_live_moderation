/* global FB:true  */
const fbHelpers = {
	fbApi: (path, method, params) => {
		return new Promise((resolve, reject) => {
			FB.api(path, method, params, res => {
				if (!res || res.error) {
					return reject(res.error || res);
				}
				resolve(res);
			});
		});
	},
	getLastLiveVideo: user => {
		return fbHelpers.fbApi(`/${user}/live_videos`, "get",
			{broadcast_status: ["LIVE"], limit: 1})
			.then(videoRes => videoRes.data.filter(video => video.id != "10154148308974641")[0]) // THERE'S A PHANTOM VIDEO STUCK WITH "LIVE" STATUS
			.catch(err => console.error(err));
	},
	/*
	getUserInfo: user => {
		return;
		const fields = "about,age_range,currency,first_name,gender,is_verified,locale,location,name";
		return fbHelpers.fbApi(`/${user}`, "get", {fields})
			.then(res => console.log(res));
	},
	*/
	getComments: (id, since) => {
		return fbHelpers.fbApi("/" + encodeURIComponent(id) + "/comments", "get",
			{
				since:new Date(since).toISOString(),
				live_filter:"no_filter",
				order: "reverse_chronological"
			})
			.then(commentRes => commentRes)
			.catch(() => {
				return false;
			});
	},
	login: () => {
		FB.login(() => {
			location.reload();
		}, {
			scope: "user_photos,user_videos"
		});
	}
};
export default fbHelpers;
