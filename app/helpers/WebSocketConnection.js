import WebSocket from "reconnecting-websocket";

class WebSocketConnection {
	constructor(url, myLoc, callback) {
		this.ws = new WebSocket("ws://cnnitouch.turner.com:8082");
		this.ws.onopen = () => {
			this.ws.send(JSON.stringify({command:"register", location:myLoc}));
			this.ws.onmessage = message => {
				const data = JSON.parse(message.data);
				if (data.msg.location === myLoc) {
					callback(data.msg.msg);
				}
			};
		};
		this.close = this.close.bind(this);
		this.send = this.send.bind(this);
	}
	close() {
		this.ws.close();
	}
	send(...props) {
		this.ws.send(...props);
	}
}
export default WebSocketConnection;
