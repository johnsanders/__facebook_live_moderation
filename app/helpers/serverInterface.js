import axios from "axios";
const editUrl = "/cake3/cnnitouch/cake-fb-facebookcomments/edit/1.json";
const dbUrl = "/apps/flashSQLinterface/read3.php?table=cake_fb_facebookcomments&where=id=1";
const logUrl = "/apps/misc/cnn_talk/fb/server/logEngagements.php";
import {encode} from "./utilities";

export const sendModeratedCommentsToServer = allComments => {
	const moderatedComments = allComments.filter(comment => comment.moderationStatus === "moderated" || comment.moderationStatus === "readyForAir");
	return axios.patch(editUrl, {id:1, data:encode(moderatedComments)});
};
export const getModeratedCommentsFromServer = () => (
	axios.get(dbUrl)
		.then(response => {
			const encodedComments = JSON.parse(response.data[0].data);
			const approvedComments = encodedComments.map(comment => {
				comment.comment = decodeURIComponent(comment.comment);
				return comment;
			});
			return({comments:approvedComments, liveUser:response.data[0].liveUser});
		})
);
export const sendLiveUserToServer = liveUser => (
	axios.patch(editUrl, {id:1, liveUser:liveUser})
);
export const sendQuestionToServer = (title, author, tab) => (
	axios.patch(editUrl, {id:1, title, author, tab})
);
export const getQuestionFromServer = () => (
	axios.get(dbUrl)
		.then( response => {
				const data = response.data[0];
				const {tab, title, author} = data;
				return {tab, title, author};
		})
);
export const logEngagementsToServer = comments => {
	const params = new URLSearchParams();
	params.set("engagements", JSON.stringify(comments));
	params.set("engagementType", "comment");
	axios.post(logUrl, params);
};
