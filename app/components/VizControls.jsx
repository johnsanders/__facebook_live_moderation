import React from "react";
import PropTypes from "prop-types";
const VizControls = (props) => (
	<div style={{float:"right"}}>
		<div className="btn-group" >
			<button
				name="tickerOn"
				className="btn btn-default"
				onClick={props.handleTickerControlClick}
			>
				Ticker On
			</button>
			<button
				name="tickerOff"
				className="btn btn-default"
				onClick={props.handleTickerControlClick}
			>
				Ticker Off
			</button>
		</div>
	</div>
);
VizControls.propTypes = {
	handleTickerControlClick: PropTypes.func.isRequired,
};
export default VizControls;
