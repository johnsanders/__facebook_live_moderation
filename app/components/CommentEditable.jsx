import React from "react";
import PropTypes from "prop-types";
import Textarea from "react-textarea-autosize";

const CommentEditable = props => (
	props.editActive ?
		<Textarea
			className={"commentEditable editable form-control"}
			value={props.editValue}
			onKeyDown={props.handleEditInteraction}
			onChange={props.handleEditInteraction}
			onBlur={props.handleEditActivate}
		/>
	:
		<div onClick={props.handleEditActivate}>
			{props.savedValue}
		</div>
);
CommentEditable.propTypes = {
	editActive: PropTypes.bool,
	savedValue: PropTypes.string,
	handleEditInteraction: PropTypes.func.isRequired,
	editValue:PropTypes.string.isRequired,
	handleEditActivate:PropTypes.func.isRequired
};
export default CommentEditable;
