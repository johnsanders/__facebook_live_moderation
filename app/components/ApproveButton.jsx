import React from "react";
import PropTypes from "prop-types";

const ApproveButton = (props) => (
	<button
		className="btn btn-sm btn-default"
		onClick={props.onClick}
		data-facebookId={props.facebookId}
		style={{border:props.moderationStatus==="moderated" || props.moderationStatus==="readyForAir"? "solid black 1px" : ""}}
	>
		<span className="glyphicon glyphicon-thumbs-up" />
	</button>
);
ApproveButton.propTypes = {
	onClick: PropTypes.func.isRequired,
	moderationStatus: PropTypes.string,
	facebookId: PropTypes.string
};

export default ApproveButton;
