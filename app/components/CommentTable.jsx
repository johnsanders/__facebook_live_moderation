import React from "react";
import CommentRowContainer from "../containers/CommentRowContainer";
import {approvalStrings} from "../helpers/utilities";
import PropTypes from "prop-types";

const CommentTable = props => (
	<table className="table table-bordered table-hover table-striped">
		<thead>
			<tr>
				<th width="3%">Status</th>
				<th width="20%">Username</th>
				<th width="5%">Time</th>
				<th width="55%">Comment</th>
				<th width="10%" className="charCount">Characters</th>
				<th width="5%" className="text-center">On Air</th>
				<th width="5%">Moderate</th>
			</tr>
		</thead>
		<tbody>
			{
				props.comments.map(comment => (
					<CommentRowContainer
						key={comment.facebookId}
						approvedString={approvalStrings[props.role][0]}
						unapprovedString={approvalStrings[props.role][1]}
						{...comment}
					/>
				))
			}
		</tbody>
	</table>
);

CommentTable.propTypes = {
	comments: PropTypes.arrayOf(PropTypes.object),
	approvedString:PropTypes.string,
	unapprovedString:PropTypes.string
};
export default CommentTable;
