import React from "react";
import UpdatingControl from "./UpdatingControl";
import NavHeader from "./NavHeader";
import PropTypes from "prop-types";

const NavModeration = props => {
	return (
		<nav className="navbar navbar-default navbar-fixed-top">
			<div className="container-fluid">
				<NavHeader />
				<form className="navbar-form navbar-left">
					<div className="form-group" style={{marginRight:"5px"}}>
						<input
							type="text"
							id="liveUser"
							className="form-control"
							value={props.liveUser}
							onChange={props.handleInputUpdate}
						/>
					</div>
					<button className="btn btn-default" onClick={props.setLiveUser}>
						Set User
					</button>
					<div className="form-group led-container" style={{width:"83px"}}>
						<div className={"led " + (props.videoId ? "led-green" : "led-red")} />
						{props.videoId ? "LIVE" : "Not Live"}
					</div>
					<div className="form-group">
						{
							props.logging ?
								<span>
									<span className="glyphicon glyphicon-flash" />
									Logging Users
								</span>
							: null
						}
						{
							props.ccConnected ?
								<span style={{marginLeft:"10px"}}>
									<span className="glyphicon glyphicon-comment" style={{marginRight:"5px"}}/>
									CC Connected
								</span>
							: null
						}
					</div>
				</form>
				{ props.videoId ?
					<form className="navbar-right">
						<UpdatingControl
							updating={props.updating}
							handleInputUpdate={props.handleInputUpdate}
						/>
					</form>
				: null }
			</div>
		</nav>
	);
};
NavModeration.propTypes = {
	updating:PropTypes.bool,
	logging:PropTypes.bool,
	ccConnected:PropTypes.bool,
	handleInputUpdate:PropTypes.func.isRequired,
	videoId:PropTypes.string,
	liveUser:PropTypes.string,
	setLiveUser:PropTypes.func.isRequired
};
export default NavModeration;
