import React from "react";
import guideImage from "../img/question_guide.jpg";

const inputs = ["Tab", "Title", "Author"];
const style = {
	marginTop:"20px",
	backgroundColor:"#D0D0D0",
	padding:"10px 0"
};
const inputStyle = {
	paddingTop:"5px",
	textAlign:"right"
};
const QuestionControls = props => (
	<div style={style} className="row">
		<h3 className="text-center">
			Ticker Question
		</h3>
		<div className="col-xs-9">
			<div>
				<form
					className="form-horizontal"
					onSubmit={props.handleSave}
				>
					{
						inputs.map( input => (
						<div key={input} className="form-group">
							<div
								style={inputStyle}
								className="col-xs-1"
							>
								<label htmlFor={input.toLowerCase()}>
									{input}
								</label>
							</div>
							<div className="col-xs-11">
								<input
									name={input.toLowerCase()}
									className="form-control"
									value={props[input.toLowerCase()]}
									onChange={props.handleChange}
								/>
							</div>
						</div>
						))
					}
					<div className="form-group">
						<div className="col-xs-offset-1 col-xs-10">
							<input
								type="submit"
								value="Save Question"
								className="btn btn-primary"
							/>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div className="col-xs-3">
			<img src={guideImage} width="100%"/>
		</div>
	</div>
);

export default QuestionControls;
