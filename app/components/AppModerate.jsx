import React from "react";
import NavModeration from "./NavModeration";
import CommentPanelContainer from "../containers/CommentPanelContainer";
import PropTypes from "prop-types";

class AppModerate extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return(
			<div>
				<NavModeration
					handleInputUpdate={this.props.handleInputUpdate}
					liveUser={this.props.liveUser}
					setLiveUser={this.props.setLiveUser}
					videoId={this.props.videoId}
					getVideoId={this.props.getVideoId}
					updating={this.props.updating}
					logging={this.props.logging}
					ccConnected={this.props.ccConnected}
				/>
				<div className="container-fluid" style={{marginTop:"65px"}}>
					<CommentPanelContainer
						role="moderate"
						videoId={this.props.videoId}
						toggleCc={this.props.toggleCc}
						ccConnected={this.props.ccConnected}
					/>
					{
						this.props.fbLoginStatus ? null :
						<div className="login">
							<button
								className="btn btn-primary"
								onClick={this.props.login}
							>
								Connect to Facebook
							</button>
						</div>
					}
				</div>
			</div>
		);
	}
}

AppModerate.propTypes = {
	handleInputUpdate:PropTypes.func.isRequired,
	liveUser:PropTypes.string,
	setLiveUser:PropTypes.func.isRequired,
	videoId:PropTypes.string,
	getVideoId:PropTypes.func.isRequired,
	updating:PropTypes.bool.isRequired,
	logging:PropTypes.bool.isRequired,
	ccConnected:PropTypes.bool.isRequired,
	toggleCc:PropTypes.func.isRequired,
	fbLoginStatus:PropTypes.bool.isRequired,
	login:PropTypes.func.isRequired
};

export default AppModerate;
