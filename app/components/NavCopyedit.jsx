import React from "react";
import NavHeader from "./NavHeader";

const NavCopyedit = () => {
	return (
		<nav className="navbar navbar-default navbar-fixed-top">
			<div className="container-fluid">
				<NavHeader />
			</div>
		</nav>
	);
};
export default NavCopyedit;
