import React from "react";
import talkLogo from "../img/talkLogo.png";
const NavHeader = () => (
	<div className="navbar-header">
		<a className="navbar-brand" href="#">
			<img src={talkLogo}/>
		</a>
	</div>
);
export default NavHeader;
