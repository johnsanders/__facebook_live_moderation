import React from "react";
import NavCopyedit from "./NavCopyedit";
import CommentPanelContainer from "../containers/CommentPanelContainer";

const AppCopyedit = () => (
	<div>
		<NavCopyedit />
		<div className="container-fluid" style={{marginTop:"65px"}}>
			<CommentPanelContainer role="copyedit" />
		</div>
	</div>
);

export default AppCopyedit;
