import React from "react";
import PropTypes from "prop-types";

const StatusIcon = (props) => {
	return (
		<span style={{letterSpacing:"-3px"}}>
			{
				props.moderationStatus === "moderated" ||
				props.moderationStatus === "readyForAir" ?
					<span className="glyphicon glyphicon-ok"></span>
					: <span className="glyphicon glyphicon-remove"></span>
			}
			{
				props.moderationStatus === "readyForAir" ?
					<span className={"glyphicon glyphicon-ok"}></span>
					: null
			}
		</span>
	);
};
StatusIcon.propTypes = {
	moderationStatus:PropTypes.string
};
export default StatusIcon;
