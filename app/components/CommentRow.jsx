import React from "react";
import CommentEditableContainer from "../containers/CommentEditableContainer";
import StatusIcon from "./StatusIcon";
import ApproveButton from "./ApproveButton";
import {timestampToHourMinSec} from "../helpers/utilities";
import PropTypes from "prop-types";

const CommentRow = props => (
	<tr style={{backgroundColor:props.getBkgColor(props.moderationStatus)}}>
		<td className="text-center">
			<StatusIcon moderationStatus={props.moderationStatus} />
		</td>
		<td>
			<CommentEditableContainer
				value={props.user}
				fieldName="user"
				facebookId={props.facebookId}
			/>
		</td>
		<td>{timestampToHourMinSec(props.created)}</td>
		<td>
			<CommentEditableContainer
				value={props.comment}
				fieldName="comment"
				facebookId={props.facebookId}
			/>
		</td>
		<td className={"charCount " + props.getCountClass(props.comment.length)}>
			{props.comment.length.toString()}
		</td>
		<td className={"text-center" + (props.onAirNow ? " onAir" : "")}>
			{props.numDisplays.toString()}
		</td>
		<td>
			<ApproveButton
				onClick={props.handleApprove}
				facebookId={props.facebookId}
				moderationStatus={props.moderationStatus}
			/>
		</td>
	</tr>
);

CommentRow.propTypes = {
	moderationStatus:PropTypes.string,
	unapprovedString:PropTypes.string,
	approvedString:PropTypes.string,
	user:PropTypes.string,
	facebookId:PropTypes.string,
	created:PropTypes.number,
	onAirNow:PropTypes.bool,
	numDisplays:PropTypes.number,
	comment:PropTypes.string,
	getBkgColor:PropTypes.func.isRequired,
	getCountClass:PropTypes.func.isRequired,
	handleApprove:PropTypes.func.isRequired
};
export default CommentRow;
