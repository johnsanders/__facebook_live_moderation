import React from "react";
import PropTypes from "prop-types";

const ModerateControls = props => (
	<div className="btn-group" style={{float:"left"}}>
		<button
			className="btn btn-default"
			onClick={props.clearApprovals}
		>
			Rescind All Approvals
		</button>
		<button
			className="btn btn-default"
			onClick={props.cleanUp}
		>
			Clean Up Unused
		</button>
		<button
			className="btn btn-default"
			onClick={props.toggleCc}
		>
			{props.ccConnected ? "Disconnect " : "Connect "}
			CC Server
		</button>
		<button
			className="btn btn-default"
			name="ticker"
			onClick={props.toggleControlVisible}
		>
			{props.tickerControlVisible ? "Hide " : "Show "}
			Viz Ticker Controls
		</button>
		<button
			className="btn btn-default"
			name="question"
			onClick={props.toggleControlVisible}
		>
			{props.tickerControlVisible ? "Hide " : "Show "}
			Question Controls
		</button>
	</div>
);
ModerateControls.propTypes = {
	clearApprovals:PropTypes.func.isRequired,
	cleanUp:PropTypes.func.isRequired,
	toggleCc:PropTypes.func.isRequired,
	ccConnected:PropTypes.bool.isRequired,
	toggleControlVisible:PropTypes.func.isRequired,
	tickerControlVisible:PropTypes.bool.isRequired
};
export default ModerateControls;
