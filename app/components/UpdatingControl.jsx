import React from "react";
import PropTypes from "prop-types";

const UpdatingControl = (props) => {
	const updateToggle = () => {
		props.handleInputUpdate({target:{id:"updating", value:!props.updating}});
	};
	return(
		<div style={{padding:"12px 12px"}}>
			<label
				className={"label " + (props.updating ? "label-success" : "label-default")}
				htmlFor="updatingSwitch"
				style={{marginRight:"5px", fontSize:"16px"}}
			>
				{ props.updating ? "Updating On " : "Updating Off " }
				<input
					type="checkbox"
					name="updating"
					onChange={updateToggle}
					checked={props.updating}
					style={{position:"relative", top:"-1px"}}
				/>

			</label>
		</div>
	);
};
UpdatingControl.propTypes = {
	updating:PropTypes.bool,
	handleInputUpdate:PropTypes.func.isRequired
};
export default UpdatingControl;
